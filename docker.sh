#!/usr/bin/env sh

DIRNAME="$(dirname "$0")"

# Check if .env file exists
if [ -e .env ]; then
    source .env
else
    APP_ENV='local'
fi

action=$1

if [ $action = 'push' ]
then
    #
    # Push to hub
    #
    if [ -z $2 ] || ( [ $2 != '7.2' ] && [ $2 != '8.0' ] && [ $2 != 'arm-8.0' ] )
    then
        echo 'Invalid php version  ( 7.2 | 8.0 | arm-8.0 )'
        exit 1
    fi

    # Check if dockerfile exist
    if [ -e $2/Dockerfile ]; then
        echo $2 ' exist'
          #
          # Push to hub
          #
          echo -n ">> Push image <$HUBE_REPOSITORY:$2> to docker hub ? (yes/no): "
          read yn_push
          if [ "$yn_push" = "yes" ]; then

            echo "Build image $HUBE_REPOSITORY"
            docker build --no-cache -t $HUBE_REPOSITORY:$2 $2

            echo "Tag image $HUBE_REPOSITORY:$2"
            docker image tag $HUBE_REPOSITORY "$HUBE_REPOSITORY:$2"

            echo "Push image $HUBE_REPOSITORY:$2"
            docker image push "$HUBE_REPOSITORY:$2"
          fi
       else
         echo $2 ' not exist'
    fi
fi